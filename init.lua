local modules = {
	"core.options",
	"core.mappings",
	"custom",
  "core.plugins",
  "core.settings",
	"core.colors"
}

for _, module in ipairs(modules) do
	local status_ok, error = pcall(require, module)
	if not status_ok then
		print("Error loading " .. module .. "\n\n".. error)
	end
end
