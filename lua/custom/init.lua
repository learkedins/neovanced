local M = {}

M.options = {
  termguicolors = true,
  laststatus = 3,
}

M.appearance = {
  theme = "catppuccin",
}

M.mappings = function() 
  require('custom.mappings')
end

M.plugin = function()
  require('custom.plugins')
end

return M
