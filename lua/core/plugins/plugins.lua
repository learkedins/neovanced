local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end

local custom_plugins = require("custom.plugins")
local plug_list = {
  { "wbthomason/packer.nvim" },
  { "catppuccin/nvim" },
  { "windwp/nvim-autopairs" },
  { "kyazdani42/nvim-web-devicons" },
  { "kyazdani42/nvim-tree.lua" },
  { "numToStr/Comment.nvim" },
  { "nvim-lua/plenary.nvim" },
  { "nvim-lua/popup.nvim" },
  { "feline-nvim/feline.nvim" },
}

return packer.startup({function(use)
  for _, v in pairs(plug_list) do 
    use(v)
  end

  for _, v in pairs(custom_plugins) do
    use(v)
  end
end,
config = {
  display = {
    open_fn = require('packer.util').float,
  }
}})
